<?php

/**
 * @file
 * Contains \Drupal\listado\ListadoController\ExampleController.
 */

namespace Drupal\listado\Controller;

use Drupal;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;


class ListadoController extends ControllerBase
{


    /**
     * Listado
     * @return array
     */
    public function listado()

    {
        // Muestro la plantilla
        return array(
            '#theme' => 'listado'
        );
    }

    public function cargarUsuarios()
    {
        $database = \Drupal::database();
        $query = $database->select('tUsuarios', 'u')
            ->fields('u')
            ->execute()
            ->fetchAll();
        return new JsonResponse($query);
    }


}




